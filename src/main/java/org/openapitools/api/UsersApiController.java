package org.openapitools.api;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.openapitools.exceptions.UnauthorizedException;
import org.openapitools.exceptions.UserAlreadyExists;
import org.openapitools.exceptions.UserNotFound;
import org.openapitools.model.*;

import java.time.OffsetDateTime;
import java.util.*;


import org.openapitools.model.Error;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.constraints.*;
import javax.validation.Valid;

import javax.annotation.Generated;

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-12-02T15:46:38.418007+01:00[Europe/Warsaw]")
@Controller
@RequestMapping("${openapi.usersCRUDInterface.base-path:/api}")
public class UsersApiController implements UsersApi {

    private final NativeWebRequest request;

    @Autowired
    public UsersApiController(NativeWebRequest request) {
        this.request = request;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<UserResponse> createUser(CreateRequest body) {
        User user = body.getUser();

        if(user.getSurname().equals("Musk")) {
            throw new UnauthorizedException();
        }

        UserResponse response = new UserResponse();
        response.setUser(user);
        response.setResponseHeader(new RequestHeader().requestId(UUID.randomUUID()).sendDate(OffsetDateTime.now()));

        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<Void> deleteUser(UUID id) {
        if (id.equals(UUID.fromString("046b6c7f-0b8a-43b9-b35d-6489e6daee91"))) {
            throw new UnauthorizedException();
        }

        User user = new User()
                .age(12)
                .id(UUID.randomUUID())
                .name("Jan")
                .surname("Kowalski")
                .email("mail@mailer.com")
                .citizenship(User.CitizenshipEnum.DE)
                .personalId("12345678909");

        user = null; // simulate deleting user

        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<UserListResponse> getAllUsers() {
        String accept = this.request.getHeader("Accept");

        if(accept == null || !accept.contains("application/json")) {
            throw new UnauthorizedException();
        }

        UserListResponse response = new UserListResponse();
        List<User> userList = new ArrayList<>();
        userList.add(new User()
                .age(12)
                .id(UUID.randomUUID())
                .name("Jan")
                .surname("Kowalski")
                .email("mail@mailer.com")
                .citizenship(User.CitizenshipEnum.DE)
                .personalId("12345678909")
        );

        response.setResponseHeader(new RequestHeader().requestId(UUID.randomUUID()).sendDate(OffsetDateTime.now()));
        response.setUsersList(userList);

        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<UserResponse> getUserById(UUID id) {
        if (!id.equals(UUID.fromString("046b6c7f-0b8a-43b9-b35d-6489e6daee91"))) {
            throw new UserNotFound();
        }

        User user = new User()
                .age(12)
                .id(UUID.fromString("046b6c7f-0b8a-43b9-b35d-6489e6daee91"))
                .name("Jan")
                .surname("Kowalski")
                .email("mail@mailer.com")
                .citizenship(User.CitizenshipEnum.DE)
                .personalId("12345678909");

        UserResponse response = new UserResponse();
        response.setUser(user);
        response.setResponseHeader(new RequestHeader().requestId(UUID.randomUUID()).sendDate(OffsetDateTime.now()));

        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<UserResponse> updateUser(UUID id, UpdateRequest body) {
        User requestUser = body.getUser();

        if(requestUser.getAge() < 18) {
            throw new UnauthorizedException();
        }

        User user = new User()
                .age(12)
                .id(UUID.randomUUID())
                .name("Jan")
                .surname("Kowalski")
                .email("mail@mailer.com")
                .citizenship(User.CitizenshipEnum.DE)
                .personalId("12345678909");

        user.setAge(requestUser.getAge());

        UserResponse response = new UserResponse();
        response.setUser(user);
        response.setResponseHeader(new RequestHeader().requestId(UUID.randomUUID()).sendDate(OffsetDateTime.now()));

        return ResponseEntity.ok(response);
    }
}
